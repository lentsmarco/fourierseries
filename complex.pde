class complex {
  float real;
  float imaginary;

  complex(float a, float b) {
    real = a;
    imaginary = b;
  }

  String toString() {
    return(real+"+"+imaginary+"i");
  }

  float mag() {
    return real*real+imaginary*imaginary;
  }
}

complex multiply(complex a, complex b) {
  return(new complex(a.real*b.real-a.imaginary*b.imaginary, a.real*b.imaginary+a.imaginary*b.real));
}

complex multiply(complex a, float b){
  return new complex(a.real*b,a.imaginary*b);
}

complex add(complex a, complex b){
  return(new complex(a.real+b.real,a.imaginary+b.imaginary));
}

complex exp(complex x){
  return multiply(new complex(cos(x.imaginary), sin(x.imaginary)), exp(x.real));
}

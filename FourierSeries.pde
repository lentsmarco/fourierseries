PImage image; // the variable in which the image is stored


float multiplier = 1.5; //when right, left, up or down arrow are pushed hor_mult and vert_mult are increased or decreased by multiplying with or dividing by this factor 
float hor_mult=1; //multiplier for horizontal edge detection
float vert_mult=1; //multiplier for vertical edge detection
int thres = 20; //threshold which the gradient multiplied by the multipliers must be above to register as edge


int brush_size = 30; //brush size of your graph drawing brush


ArrayList <PVector> graph = new ArrayList(); //the list where your drawn path is stored
float len = 0; //how long the graph is


ArrayList <PVector> decomposition = new ArrayList(); //the list where the calculated coefficients for the fourier decomposition are stored
ArrayList <PVector> series = new ArrayList(); //the list where the resulting path of the fourier series is stored



complex shift = new complex(1500, 500); //where the fourier series starts on the screen
int n = 2; //how many fourier terms are added together from 2^(n-1) to 2^(n-1)-1 (increased by '+' decreased by '-')


float x = 0; //variable where the shown curve is in its path
float speed = 1; //constant to change how fast x is changing (increased by 'd' decreased by 'a')


boolean hide = false; //hide background image, path and brush





PImage gradient(PImage in) { //computes the gradient of each pixel with respect to its neighboring pixels
  int w = in.width-2;
  int h = in.height-2;
  PImage out = new PImage(w, h);
  for (int i=1; i < w; i++) {
    for (int j=1; j < h; j++) {
      //next lines are just gradient formula for each component
      float red_hor = abs(red(in.pixels[i+j*w])-red(in.pixels[i+j*w-1]));
      float red_vert = abs(red(in.pixels[i+j*w])-red(in.pixels[i+(j-1)*w]));
      float green_hor = abs(green(in.pixels[i+j*w])-green(in.pixels[i+j*w-1]));
      float green_vert = abs(green(in.pixels[i+j*w])-green(in.pixels[i+(j-1)*w]));
      float blue_hor = abs(blue(in.pixels[i+j*w])-blue(in.pixels[i+j*w-1]));
      float blue_vert = abs(blue(in.pixels[i+j*w])-blue(in.pixels[i+(j-1)*w]));
      //multiply horizontal and vertical components by its multypliers add them and compare to thres
      if (dist(0, 0, 0, red_hor, green_hor, blue_hor)*hor_mult+dist(0, 0, 0, red_vert, green_vert, blue_vert)*vert_mult>thres) {
        out.pixels[i-1+j*(w-2)] = color(255);
      } else {
        out.pixels[i-1+j*(w-2)] = color(0);
      }
    }
  }
  return out;
}


int[] getBits(int data){
  int nBits = ceil(log(data+1)/log(2));
  int[] out = new int[nBits];
  for(int i = 0; i < nBits; i++){
    out[i] = (data & (1<<i)) >> i;
  }
  return out;
}


int[] getBits(int data, int len){
  int nBits = ceil(log(data+1)/log(2));
  if(len < nBits){
    println("to few bits");
    exit();
  }
  nBits = len;
  int[] out = new int[nBits];
  for(int i = 0; i < nBits; i++){
    out[i] = (data & (1<<i)) >> i;
  }
  return out;
}

complex[] fft(complex[] g_in) { // https://de.wikipedia.org/wiki/Schnelle_Fourier-Transformation
  // permute the elements
  int len =g_in.length;
  complex[] g = new complex[len];
  int nBits = ceil(log(len)/log(2));
  
  for(int i = 0; i < len; i++) {
    int[] bits = getBits(i, nBits);
    int newPos = 0;
    for(int j = 0; j < nBits; j++){
      newPos += bits[nBits - j - 1] << j;
    }
    g[newPos] = g_in[i];
  }
  
  complex[] g_temp = new complex[len];
  
  for(int layer = 0; layer < nBits; layer++){
    for(int section = 0; section < len / (1 << (layer+1)); section++)
    for(int element = 0; element < 1 << layer; element++){
      int left = (1 << layer+1) * section + element;
      int right = left + (1 << layer);
      g_temp[left] = add(g[left], multiply(exp(new complex(0.0, -PI*element/(1 << layer))), g[right]));
      g_temp[right] = add(g[left], multiply(multiply(exp(new complex(0.0, -PI*element/(1 << layer))), g[right]), -1)); 
    }
    for(int i = 0; i < len; i++) {
      g[i] = g_temp[i];
    }
  }
  return g;
}


complex[] sample(ArrayList <PVector> g, int n){ // sample the graph at 2^n points
  g.add(g.get(0));
  float[] cumulativeDistances = new float[g.size()]; // the distances of the points from the start
  float[] distances = new float[g.size()-1];
  float total_length = 0;
  int nPoints = 1 << n;
  for(int i = 0; i < g.size()-1; i++){
    float d = dist(g.get(i).x, g.get(i).y, g.get(i+1).x, g.get(i+1).y);
    cumulativeDistances[i] = total_length;
    distances[i] = d;
    total_length += d;
  }
  cumulativeDistances[g.size()-1] = total_length;
  
  float step = total_length / nPoints;
  float done = 0;
  int currentPoint = 0;
  complex[] samples = new complex[nPoints];
  samples[0] = pixelToComplex(g.get(0));
  
  for(int i = 1; i < nPoints; i++) {
    while(done > cumulativeDistances[currentPoint+1]){
      currentPoint++;
    }
    float positionOnSection = (done - cumulativeDistances[currentPoint]) / distances[currentPoint]; // normalized to be in (0, 1)
    PVector currentSample = new PVector(map(positionOnSection, 0, 1, g.get(currentPoint).x, g.get(currentPoint+1).x), map(positionOnSection, 0, 1, g.get(currentPoint).y, g.get(currentPoint+1).y));
    samples[i] = pixelToComplex(currentSample);
    done += step;
  }
  g.remove(g.get(g.size()-1));
  return samples;
}

complex pixelToComplex(PVector pos){
  float real = pos.x - shift.real;
  float imaginary = pos.y - shift.imaginary;
  return new complex(real, imaginary);
}



complex[] fourierDecompose(ArrayList <PVector> g, int n) {
  complex[] samples = sample(g, n);
  complex[] coeffs = fft(samples);
  return coeffs;
}



void keyPressed() { // ux functionality
  if (key==CODED) {
    if (keyCode == UP) {
      vert_mult*=multiplier;
    }
    if (keyCode == DOWN) {
      vert_mult/=multiplier;
    }
    if (keyCode == LEFT) {
      hor_mult/=multiplier;
    }
    if (keyCode == RIGHT) {
      hor_mult*=multiplier;
    }
  }
  if (key==' ') {
    for (int i=0; i<10; i++) {
      graph.remove(graph.get(graph.size()-1));
    }
  }
  if (key == 'g') { //generate the fourier decomposition
    decomposition = new ArrayList();
    complex[] fft = fourierDecompose(graph, n);
    for (int i=1<<(n-1); i<1<<n; i++ ) {
      int index = ((i+(1<<(n-1)))%(1<<n))-(1<<(n-1));
      complex decomposition_component = multiply(fft[i], 1/float(1<<n));
      decomposition.add(new PVector(decomposition_component.real, decomposition_component.imaginary, index));
    }
    for (int i=0; i<1<<(n-1); i++ ) {
      int index = ((i+(1<<(n-1)))%(1<<n))-(1<<(n-1));
      complex decomposition_component = multiply(fft[i], 1/float(1<<n));
      decomposition.add(new PVector(decomposition_component.real, decomposition_component.imaginary, index));
    }
  }
  if (key=='r') { //render the graph which the series will trace
    series=new ArrayList();
    for (float j=0; j<=1; j+=speed/float(graph.size()+1)) {
      complex tip = shift;
      for (int i=0; i<decomposition.size(); i++ ) {
        complex number=multiply(new complex(decomposition.get(i).x, decomposition.get(i).y), new complex(cos(2*PI*decomposition.get(i).z*j), sin(2*PI*decomposition.get(i).z*j)));
        tip = add(tip, number);
      }
      series.add(new PVector(tip.real, tip.imaginary));
    }
  }
  if (key == 'h') {
    hide=!hide;
  }
  if (key == '+') {
    n++;
    println("now using " + round(pow(2,n)) + " coefficients");
  }
  if (key == '-') {
    n--;
    println("now using " + round(pow(2,n)) + " coefficients");
  }
  if (key == 'd') {
    speed*=1.5;
  }
  if (key == 'a') {
    speed/=1.5;
  }
}










void setup() {
  
  background(255);
  size(3000, 1000);
  
  
  image = loadImage("image5.jpg");
  image.filter(BLUR, 1); //helps with noise
}





void draw() {
  background(0);
  
  
  PImage gradient = gradient(image);
  
  
  if (!hide) {
    image(gradient, 0, 0);
    
    //brush around mousecursor
    noFill();
    stroke(255);
    ellipse(mouseX, mouseY, brush_size, brush_size);
    
    //draw the graph
    for (int i=0; i< graph.size(); i++) {
      noStroke();
      fill(color(255, 0, 0));
      ellipse(graph.get(i).x, graph.get(i).y, 10, 10);
      stroke(color(255, 0, 0));
      line(graph.get((i+1)%graph.size()).x, graph.get((i+1)%graph.size()).y, graph.get(i).x, graph.get(i).y);
    }
  }
  
  //draw the path the series traces
  for (int i=0; i< series.size(); i++) {
    stroke(255);
    line(series.get((i+1)%series.size()).x, series.get((i+1)%series.size()).y, series.get(i).x, series.get(i).y);
  }
  
  //draw the series 
  complex tip = shift;
  for (int i=0; i<decomposition.size(); i++ ) {
    complex number=multiply(new complex(decomposition.get(i).x, decomposition.get(i).y), new complex(cos(2*PI*decomposition.get(i).z*x), sin(2*PI*decomposition.get(i).z*x)));
    noFill();
    stroke(255);
    ellipse(tip.real, tip.imaginary, 2*sqrt(number.mag()), 2*sqrt(number.mag()));
    tip = add(tip, number);
    fill(255);
    ellipse(tip.real, tip.imaginary, 5, 5);
  }
  noStroke();
  fill(color(0, 255, 0));
  ellipse(tip.real, tip.imaginary, 10, 10);



  //add new graph nodes if the mouse is pressed
  if (mousePressed) {
    PVector center = new PVector();
    int count = 0;
    int w = gradient.width;
    //average over the white pixels in the brush size to  "get attracted" by edges
    for (int i=-brush_size/2; i< brush_size/2; i++) {
      for (int j=-brush_size/2; j< brush_size/2; j++) {
        try {
          if (brightness(gradient.pixels[mouseX+i+(mouseY+j)*w])>2) {
            center.add(new PVector(mouseX+i, mouseY+j));
            count++;
          }
        }
        catch(Exception e) {
        }
      }
    }
    if (count > 0) {
      graph.add(center.div(count));
    }
  }
  
  //move along the path
  x+=speed/float(graph.size()+1);
  x=x%1;
}

This processing sketch (https://processing.org/) lets you load an image, tries to detect edges and creates a fourier-series based on a path you draw in it.

How to use:

in the setup function change your window size and image filename

run the sketch

adjust vertical and horizontal gradient multipliers to get nice edge detection by pressing left, right, up and downarrow and trace your graph while pressing your mouse

press space to remove the last 10 nodes

press 'g' to generate the fourier coefficients

press 'r' to render the path this fourier series traces

press '+' or '-' to increase or decrease the number terms in your fourier series

press 'a' or 'd' to increase or derease the speed the fourier series moves

hope you enjoy